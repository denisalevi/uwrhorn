mqtt_config["topics"] = ["tournament/delay",
                         "tournament/gameday",
                         "tournament/roster",
                         "tournament/lineup",
                         "state/real_time",
                         "state/game_time",
                         "state/game_state",
                         "state/game_config",
                         "command/answer",
                         "command/print_answer"];

$(document).ready(function() {
    $.fn.editable.defaults.mode = 'inline';
    $.fn.editable.defaults.showbuttons = false;
    $.fn.editable.defaults.unsavedclass = null;

    $gamedaytable = $('#gamedaytable');
    // set options for gameday table
    $gamedaytable.bootstrapTable({
        classes: "table table-hover table-no-bordered",
        iconsPrefix: 'fa',
        idField: 'id',
        uniqueId: 'id',
        undefinedText: '',
        search: true,
        searchAlign: 'left',
        trimOnSearch: false,
        striped: true,
        sortName: 'id',
        sortOrder: 'asc',
        columns: create_columns_list(),
        onClickCell: void(0),
        escape: true,
        toolbar: '#toolbar',
        toolbarAlign: 'right',
        rowStyle: rowStyle
    });
    $gamedaytable.bootstrapTable('showLoading');

    $('#lineupModal').on('hidden.bs.modal', saveLineUpPlayer);
    $('#teamsModal').on('hidden.bs.modal', saveTeamsSetup);
    $('#playersModal').on('hidden.bs.modal', setTeamsModal);

    $gamedaytable.on('editable-shown.bs.table', function (event, field, row, $e, editable) {
        gamedaytable_on_editing = true;
        return false;
    });
    $gamedaytable.on('editable-hidden.bs.table', function (event, field, row, $e, reason) {
        gamedaytable_on_editing = false;
        return false;
    });
    $gamedaytable.on('editable-save.bs.table', function (event, field, row, oldValue, $e) {
        let id = $e.data('pk');
        for (let index = 0; index < gameday.length; index++) {
            if (gameday[index].id == id) {
                gameday[index][field] = row[field];
                mqtt_messages.send("tournament/gameday", JSON.stringify(gameday));
            }
        }
        return false;
    });

    MQTTconnect();
    TeamsModal_add_enter();
    PlayersModal_add_enter();
});

var edit_team_formatter = function (value, row, index, field) {  
    let out = teamname_formatter(value, row, index, '');
    return ['<a href="javascript:void(0)"',
            ' data-name="' + field + '"',
            ' data-pk="' + row.id + '"',
            ' data-value="' + value + '"',
            '>' + out + '</a>'].join('');
};

var validate_team = function (value, element){
    let id = $(element).data('pk');
    let field = $(element).data('name');
    for (let index = 0; index < gameday.length; index++) {
        if (gameday[index].id == id) {
            if (field in gameday[index]) {
                gameday[index][field] = value;
                passed = true;
                return null;
            }
        }
    }
    return 'Unknown Team';
};

var validate_plan = function (value, element) {
    let id = $(element).data('pk');
    let dt = moment(value, 'DD.MM.YYYY HH:mm').utc();
    if (dt.isValid()) {
        let out = dt.format();
        for (let index = 0; index < gameday.length; index++) {
            if (gameday[index].id == id) {
                if (out != gameday[index]['plan']) {
                    gameday[index]['plan'] = out;
                }
                return {newValue: out};
            }
        }
    }
    return 'Wrong time format.';
};


function lineup_formatter(value, row, index, team_color) {
    let params = "'" + row['id'] + "', '" + team_color + "', '" + row[team_color] + "'";
    let out = '<button type="submit" class="btn btn-light py-1 px-3 my-1" onclick="setLineupModal(' + params + ')">';
    out += '<i class="fa fa-bars uwr-icons"></i></button>';
    return out;
};
    
function actionbuttons_formatter(value, row, index) {
    let game_id = "'" + row['id'] + "'";
    let out = `
<button type="submit" class="btn btn-light py-1 px-3 m-1" onclick="deleteGame(${game_id})">
    <i class="fa fa-trash uwr-icons" ></i>
</button>
<button type="submit" class="btn btn-light py-1 px-3 m-1" onclick="printGame(${game_id})">
    <i class="fa fa-print uwr-icons" ></i>
</button>`;
    return out;
};

var create_columns_list = function () {
    let values = [];
    $.each(roster, function (key, value){
        values.push({'value': key,
                     'text': value.name});
    });
    let columns = [
        {field: 'id',
         title: '#',
         sortable: true},
        {field: 'plan',
         title: 'Plan',
         sortable: true,
         formatter: plan_formatter,
         editable: {
             type: 'text',
             validate: function(v) {
                 return validate_plan(v, this); }}},
        {field: 'time',
         title: 'Time',
         formatter: time_formatter,
         // editable: {
         //     type: 'text',
             //noeditFormatter: time_formatter,
             // dispaly: function (v) {
             //     time_dispaly(v, this)
            // }
        },
        {field: 'bluelineup',
         title: '',
         formatter: function (v, r, i) {
             return lineup_formatter(v, r, i, 'blue'); },
         width: 20},
        {field: 'blue',
         title: 'Blue',
         editable: {
             type: 'select',
             source: values,
             noeditFormatter: function (v, r, i) {
                 return edit_team_formatter(v, r, i, 'blue'); },
             validate: function (v) {
                 return validate_team(v, this); }}},
        {field: 'whitelineup',
         title: '',
         formatter: function (v, r, i) {
             return lineup_formatter(v, r, i, 'white'); },
         width: 20},
        {field: 'white',
         title: 'White',
         editable: {
             type: 'select',
             source: values,
             noeditFormatter: function (v, r, i) {
                 return edit_team_formatter(v, r, i, 'white'); },
             validate: function (v) {
                 return validate_team(v, this); }}},
        {field: 'main_ref',
         title: 'Main Ref',
         editable: {
             type: 'select',
             source: values,
             noeditFormatter: function (v, r, i) {
                 return edit_team_formatter(v, r, i, 'main_ref'); },
             validate: function (v) {
                 return validate_team(v, this); }}},
        {field: 'uw_ref_1',
         title: 'UW Ref 1',
         editable: {
             type: "select",
             source: values,
             noeditFormatter: function (v, r, i) {
                 return edit_team_formatter(v, r, i, 'uw_ref_1'); },
             validate: function (v) {
                 return validate_team(v, this); }}},
        {field: 'uw_ref_2',
         title: 'UW Ref 2',
         editable: {
             type: 'select',
             source: values,
             noeditFormatter: function (v, r, i) {
                 return edit_team_formatter(v, r, i, 'uw_ref_2'); },
             validate: function (v) {
                 return validate_team(v, this); }}},
        {field: 'score',
         title: 'Score'},
        {field: 'actionbuttons',
         title: '',
         formatter: actionbuttons_formatter}];
    return columns;
};

function rowStyle(row, index) {
    if (!game_config) return {};
    if (row.id == game_config.game_id) {
        return {classes: 'table-warning'};
    }
    return {};
}

mqtt_messages.onGameday = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameday.call(this, topic, payload);
    $gamedaytable.bootstrapTable('load', gameday);
    $gamedaytable.bootstrapTable('hideLoading');
    $('.AddGameBtn').prop("disabled", false);
    $('#gamedaytable tr').removeClass('table-warning');
    if (game_config) {
        $('#gamedaytable tr[data-uniqueid="' + game_config.game_id +  '"]').addClass('table-warning');
    }
};

mqtt_messages.onRoster = function (topic, payload) {
    MQTTdefaultMessages.prototype.onRoster.call(this, topic, payload);
    //$gamedaytable.bootstrapTable('load', gameday);
    $gamedaytable.bootstrapTable('refreshOptions',
                                 {columns: create_columns_list()});
    $('.SetupTeamsBtn').prop("disabled", false);
};

mqtt_messages.onGameConfig = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameConfig.call(this, topic, payload);
    $gamedaytable.bootstrapTable('load', gameday);
};

mqtt_messages.onPrintAnswer = function (topic, payload) {
    MQTTdefaultMessages.prototype.onPrintAnswer.call(this, topic, payload);
};

function addGame() {
    // find maximum id + 1 and maximum planned time
    let new_id = 1;
    let max_time = null;
    gameday.forEach(function(game) {
        if (game["id"] >= new_id) {
            new_id = game["id"] + 1;
        }
        let dt = moment.utc(game["plan"]);
        if (dt.isValid()) {
            if (max_time === null) {
                max_time = dt;
            }
            else {
                max_time = moment.max(max_time, dt);
            }
        }
    });
    // no valid plan time defined in other games
    if (max_time === null) {
        max_time = moment.utc();
    }
    let new_game = {
        "id": new_id,
        "blue": "",
        "white": "",
        "main_ref": "",
        "uw_ref_1": "",
        "uw_ref_2": "",
        "plan": max_time.format(),
        "time": "",
        "score": ""};
    gameday.push(new_game);
    mqtt_messages.send("tournament/gameday", JSON.stringify(gameday));
};

function deleteGame(game_id) {
    if (confirm("Are you sure you want to delete the game with the id " + game_id + "?")) {
        if (game_state && game_config.game_id == game_id) {
            my_alert('You cannot remove the game that is currently running.');
            return;
        }
	    let index = 0;
	    for (index = 0; index < gameday.length; index++) {
		    if (gameday[index]["id"] == game_id) {
			    break;
		    }
	    }
	    gameday.splice(index, 1);
        mqtt_messages.send("tournament/gameday", JSON.stringify(gameday));
    }
}


function printGame(game_id) {
    let myObj = {'game_id': game_id,
                 'mqtt_client_id': MQTTclient.clientId};
    mqtt_messages.send("command/print", JSON.stringify(myObj));
}



/*
###########################
# Function for: TeamsModal
###########################
*/

function setTeamsModal() {
    let teamname_list = "";
    for (let team_id in roster) {
        teamname_list += '<div class="row  align-items-center">';
        teamname_list += '<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">';
        teamname_list += roster[team_id]["name"];
        teamname_list += '</div>';
        teamname_list += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">';
        teamname_list += '<button type="button" class="btn btn-light" onclick="setPlayersModal(\'' + team_id + '\');">';
        teamname_list += '<i class="fa fa-pencil uwr-icons"></i></button>';
        teamname_list += '<button type="button" class="btn btn-light" onclick="deleteTeamName(\'' + team_id + '\');">';
        teamname_list += '<i class="fa fa-trash uwr-icons"></i></button>';
        teamname_list += '</div>';
        teamname_list += '</div>';
    };
    $("#teamname_list").html(teamname_list);
    $("#teamsModal").modal("show");
};

function addTeamName() {
    let team = $("#teamname_list_addTeam_input").val();
    if (typeof team == "string" && team != '') {
        let team_id = team.replace(/[^a-zA-Z0-9.-]/g, '').toLowerCase();
        if (team_id in roster) {
            alert("This team already exists.");
        }
        else {
            roster[team_id] = {"name": team,
                               "players": {}};
            setTeamsModal();
        };
        $("#teamname_list_addTeam_input").val('');
    };
};

function deleteTeamName(team_id) {
    if (confirm('Are you sure you want to delete the the team "' + roster[team_id]["name"] + '"?\n This will also delete all configured lineups for this team.')) {
        delete roster[team_id];
        for (let game_id in lineup) {
            delete lineup[game_id][team_id];
        };
        setTeamsModal();
    };
};

function saveTeamsSetup() {
    mqtt_messages.send("tournament/lineup", JSON.stringify(lineup));
    mqtt_messages.send("tournament/roster", JSON.stringify(roster));
};

function TeamsModal_add_enter() {
    $('#teamname_list_addTeam_input, #teamname_list_addTeam_button').keypress(function(e){
        if(e.which == 13) {
            e.preventDefault();
            addTeamName();
        }
    });
};

/*
###############################
# Function for: PlayersModal
###############################
*/

function setPlayersModal(team_id) {
    $('#playersModal').data('team_id', team_id);
    $('#playersModal_teamname_input').val(roster[team_id]["name"]);
    list_body = "";
    for (var id in roster[team_id]["players"]) {
        var name = roster[team_id]["players"][id]["name"];
        var passnumber = roster[team_id]["players"][id]["passnumber"];
        list_body += '<div class="row form-group align-items-center">';
        list_body += '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">';
        list_body += id;
        list_body += '</div>';
        list_body += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">';
        list_body += '<input type="text" id="playersModal_name_input_' + id + '" class="form-control" placeholder="Name" value="' + name + '" onblur="updatePlayerName(\'' + id +'\');">';
        list_body += '</div>';
        list_body += '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">';
        list_body += '<input type="text" id="playersModal_pass_input_' + id + '" class="form-control" placeholder="Pass #" onkeypress="return isNumber(event)" value="' + passnumber + '" onblur="updatePlayerPassnumber(\'' + id +'\');">';
        list_body += '</div>';
        list_body += '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">';
        list_body += '<button type="button" class="btn btn-light" onclick="deletePlayer(\'' + id +'\');">';
        list_body += '<i class="fa fa-trash uwr-icons"></i></button>';
        list_body += '</div></div>';
    };
    $('#playersModal_list_body').html(list_body);
    $("#playersModal").modal("show");
};

function addNewPlayer() {
    var team_id = $('#playersModal').data('team_id');
    var name = $('#playersModal_name_input').val();
    var id = $('#playersModal_cap_input').val();
    var passnumber = $('#playersModal_pass_input').val();
    if (id in roster[team_id]["players"]) {
        alert("A player with the id '" + id + "' already exists in this team. Delete or modify this player.");
    }
    else if (!isInt(id) || id <= 0 || id >= 100) {
        alert("The cab number needs to be between 1 and 99.");
    }
    else {
        roster[team_id]["players"][id] = {};
        roster[team_id]["players"][id]["name"] = name;
        roster[team_id]["players"][id]["passnumber"] = passnumber;
        $('#playersModal_name_input').val('');
        $('#playersModal_cap_input').val('');
        $('#playersModal_pass_input').val('');
        setPlayersModal(team_id);
    }
    $('#playersModal_cap_input').focus();
};

function isInt(value) {
    return !isNaN(value) &&
        parseInt(Number(value)) == value &&
        !isNaN(parseInt(value, 10));
};

function updatePlayerName(id) {
    var team_id = $('#playersModal').data('team_id');
    var new_name = $('#playersModal_name_input_' + id).val();
    if (id in roster[team_id]["players"]) {
        roster[team_id]["players"][id]["name"] = new_name;
    }
    else {
        alert("A player with the id " + id + " does not exist.");
    }
};

function updatePlayerPassnumber(id) {
    var team_id = $('#playersModal').data('team_id');
    var new_passnumber = $('#playersModal_pass_input_' + id).val();
    if (id in roster[team_id]["players"]) {
        roster[team_id]["players"][id]["passnumber"] = new_passnumber;
    }
    else {
        alert("A player with the id " + id + " does not exist.");
    }
};

function updateTeamName() {
    var team_id = $('#playersModal').data('team_id');
    roster[team_id]["name"] = document.getElementById('playersModal_teamname_input').value;
};

function deletePlayer(id) {
    var team_id = $('#playersModal').data('team_id');
    if (id in roster[team_id]["players"]) {
        delete roster[team_id]["players"][id];
        setPlayersModal(team_id);
    }
    else {
        alert("A player with the id " + id + " does not exist.");
    }
};

function PlayersModal_add_enter(){
        $('#playersModal_cap_input, #playersModal_name_input, #playersModal_pass_input').keypress(function(e){
        if(e.which == 13) {
            e.preventDefault();
            addNewPlayer();
        }
    });
};

/*
###########################
# Function for: LineupModal
###########################
*/

function saveLineUpPlayer(){
    let cur_id   = $('#form-gameid').val();
    let cur_team = $('#form-team').data('team_id');
    let water = parseInt($('#sum-water').html());
    let sub = parseInt($('#sum-sub').html());
    if (water > 12) {
        alert("ERROR! You have more then 12 players in the water");
        $('#lineupModal').modal('show');
    }
    if (sub > 3) {
        alert("ERROR! You have more then 3 substitutes");
        $('#lineupModal').modal('show');
    }
    lineup[cur_id][cur_team].captain = $('#form-captain').val();
    lineup[cur_id][cur_team].leader = $('#form-leader').val();
    mqtt_messages.send("tournament/lineup", JSON.stringify(lineup));
};

function uptdateLineUpPlayer(id, team, player, state){
    let water = parseInt($('#sum-water').html());
    let sub = parseInt($('#sum-sub').html());
    let out = parseInt($('#sum-out').html());
    if ( lineup[id][team].member[player] != state) {
        switch(lineup[id][team].member[player]) {
        case "water":
            water--;
            break;
        case "out":
            out--;
            break;
        case "sub":
            sub--;
            break;
        default:
            break;
        }
        lineup[id][team].member[player] = state;
        switch(state) {
        case "water":
            water++;
            break;
        case "out":
            out++;
            break;
        case "sub":
            sub++;
            break;
        default:
            break;
        }
        $('#sum-water').html(water);
        $('#sum-sub').html(sub);
        $('#sum-out').html(out);
    }
};

function setLineupModal (id, color, team){
    $('#form-gameid').val(id);
    $('#form-color').val(color);
    $('#form-team').data('team_id', team);
    if (team in roster) {
        $('#form-team').val(roster[team]["name"]);
    }
    else {
        alert("Select a team fist.");
        return;
    }

    let game;
    for (let i = 0; i < gameday.length; ++i) {
        if (id == gameday[i]['id']) {
            game = gameday[i];
        }
    }
    $('#form-plantime').val(moment.utc(game['plan']).local().format("DD.MM.YYYY HH:mm"));

    // check, if there is already a line up, or if it is possible to set one up
    if (typeof lineup[id] == "undefined"){
        lineup[id] = {};
    }
    if (typeof lineup[id][team] == "undefined"){
        lineup[id][team] = {member:{},
                            captain:"",
                            leader:""};
    }
    $('#form-captain').val(lineup[id][team].captain);
    $('#form-leader').val(lineup[id][team].leader);

    // add all players from the roster to the lineup
    for (var i in roster[team]["players"]) {
        if (!(i in lineup[id][team].member)) {
            lineup[id][team].member[i] = "out";
        }
    }
    var tablerows = "";
    $('#form-captain').empty();
    let player = 0;
    let water = 0;
    let out = 0;
    let sub = 0;
    for (let i in lineup[id][team].member) {
        // check if a player in the lineup is still in the roster
        let player_name = 'Player ' + i;
        let player_passnumber = '';
        if (i in roster[team]["players"]) {
            player_name = roster[team]["players"][i]["name"];
            player_passnumber =  roster[team]["players"][i]["passnumber"];
        }
        player++;

        tablerows += '<div class="row"><div class="col-2 text-right"><p>';
        tablerows += i;
        tablerows += '</p></div><div class="col-6"><p>';
        tablerows += player_name;
        tablerows += " (";
        tablerows += player_passnumber;
        tablerows += ")";
        tablerows += '</p></div><div class="col-1 text-center"><input type="radio" name="';
        tablerows += "player_" + i;
        tablerows += '" value="water" ';
        if (lineup[id][team].member[i] == "water") {
            tablerows += "checked ";
            water++;
        }
        tablerows += "onClick=uptdateLineUpPlayer('"+id+"','"+team+"','"+i+"','water')";
        tablerows += ' /></div><div class="col-1 text-center"><input type="radio" name="';
        tablerows += "player_" + i;
        tablerows += '" value="sub" ';
        if (lineup[id][team].member[i] == "sub") {
            tablerows += "checked ";
            sub++;
        }
        tablerows += "onClick=uptdateLineUpPlayer('"+id+"','"+team+"','"+i+"','sub')";
        tablerows += ' /></div><div class="col-1 text-center"><input type="radio" name="';
        tablerows += "player_" + i;
        tablerows += '" value="out" ';
        if (lineup[id][team].member[i] == "out"){
            tablerows += "checked ";
            out++;
        }
        tablerows += "onClick=uptdateLineUpPlayer('"+id+"','"+team+"','"+i+"','out')";
        tablerows += ' /></div></div>';
        if (i == lineup[id][team].captain) {
            $('#form-captain').append($('<option>', {
                value: i,
                selected: "",
                text: i + " " + player_name
            }));
        } else {
            $('#form-captain').append($('<option>', {
                value: i,
                text: i + " " + player_name
            }));
        }

    }
    $('#player_list').html(tablerows);
    $('#sum-player').html(player);
    $('#sum-water').html(water);
    $('#sum-sub').html(sub);
    $('#sum-out').html(out);

    // open the modal after successfull setup
    $('#lineupModal').modal('show');
};
