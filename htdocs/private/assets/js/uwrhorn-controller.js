/*
##########################
# Listening and Updating
##########################
*/

mqtt_config["topics"] = ["tournament/gameday",
                         "tournament/lineup",
                         "tournament/roster",
                         "state/real_time",
                         "state/game_time",
                         "state/game_state",
                         "state/game_config",
                         "state/protocol",
                         "state/linked_timers",
                         "command/answer"];

var new_protocol_event = {};
var is_new_event = true;
var event_descriptions = {
    'goal': 'Goal',
    'timeout': 'Timeout',
    'penaltythrow': 'Penalty Throw',
    'freethrow': 'Free Throw',
    'warning': 'Warning',
    'teamwarning': 'Team Warning',
    'penaltytime': 'Penalty Time',
    'doublepenaltytime': 'Double Penalty Time',
    'matchpenalty': 'Match Penalty',
    'teamball': 'Team Ball',
    'playerexchange': 'Player Exchange',
    'refereeball': 'Referee Ball',
    'comment': 'Comment'};

// for these events ask for a team color
var need_teamcolor = ['penaltytime', 'doublepenaltytime', 'matchpenalty',
                      'freethrow', 'warning', 'teamwarning',
                      'timeout', 'playerexchange', 'goal',
                      'teamball', 'penaltythrow'];

// these events are linked to the gametimer
var with_linked_timer = ['penaltytime', 'doublepenaltytime', 'matchpenalty'];

// these events are for a team, eg. goal for blue. All other events are against,
// eg. penalty throw against blue.
var event_for_team =  ['goal', 'timeout', 'teamball', 'playerexchange'];


var touchEvent;

$(document).ready(function() {
    MQTTconnect();
    // First we check if you support touch, otherwise it's click:
    // Then we bind via that event.
    // This way we only bind one event, instead of the two for touch and click.
    touchEvent = 'ontouchstart' in window ? 'touchstart' : 'click';
    add_event_listeners();
    $('#commentModal').on('hidden.bs.modal', save_desc);
});

mqtt_messages.onGameState = function (topic, payload) {
    let old_game_state = game_state
    MQTTdefaultMessages.prototype.onGameState.call(this, topic, payload);
    if (game_state.score_blue != $('#score_blue').html()) $('#score_blue').html(game_state.score_blue);
    if (game_state.score_white != $('#score_white').html()) $('#score_white').html(game_state.score_white);
    let statetext = "H"+game_state.n_halftime;
    if (statetext !=  $('#HalfTime').html()) $('#HalfTime').html(statetext);
    if (game_state.is_timeout) {
        statetext = "Timeout";
    } else if (game_state.is_penaltythrow) {
        statetext = "Penalty Throw";
    } else if (game_state.is_halftime_break) {
        statetext = "Halftime Break";
    } else if (game_state.is_match_over) {
        statetext = "Ended";
    } else {
        statetext = "";
    }
    if (statetext != $('#game_state_text').html()) $('#game_state_text').html(statetext);
    if (game_state.is_gametime_running){
        $('#GameStatus').removeClass("fa-stop-circle GameStatusRed").addClass("fa-play-circle GameStatusGreen");
    }
    else{
        $('#GameStatus').removeClass("fa-play-circle GameStatusGreen").addClass("fa-stop-circle GameStatusRed");
    }
    let extra_html = "Add Extra Halftime";
    if (game_state.is_extra_halftime) extra_html = "Remove Extra Halftime";
    if (extra_html != $("#ToggleExtraHalftime").html()) $("#ToggleExtraHalftime").html(extra_html);
    update_linked_timers();
    if (old_game_state && game_state) {
        if (game_state.is_halftime_break) {
            // do nothing here
        } else if (game_state.is_match_over) {
            // open end of game modal, if old_game_state was not match over
        } else if (!game_state.is_gametime_running) {
            if (old_game_state.is_gametime_running) {
                // a normal game stop: open the select event modal
                if (!($("#commentModal").hasClass('show') || $("#teamcolorModal").hasClass('show') || $("#selectEventModal").hasClass('show'))) {
                    let last_game_id = protocol[protocol.length-1].id;
                    open_select_event_modal(last_game_id);
                }
            }
        }
    }
};

mqtt_messages.onGameTime = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameTime.call(this, topic, payload);
    if (game_time != $('#game_time').html()) {
        $('#game_time').html(game_time);
    }
    update_linked_timers();
};

mqtt_messages.onGameConfig = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameConfig.call(this, topic, payload);
    $('#game_length').val(game_config.game_length);
    $('#extra_game_length').val(game_config.extra_game_length);
    $('#penaltytime_length').val(game_config.penaltytime_length);
    $('#penaltythrow_length').val(game_config.penaltythrow_length);
    $('#timeout_length').val(game_config.timeout_length);
    $('#matchpenalty_length').val(game_config.matchpenalty_length);
    $('#halftime_break_length').val(game_config.halftime_break_length);
    $('#n_halftime_total').val(game_config.n_halftime_total);
    if (game_config.continuous_time) {
        $('#continuous_time').prop('checked', 'checked');
        $('#stopped_time').removeProp('checked');
    }
    else {
        $('#continuous_time').removeProp('checked');
        $('#stopped_time').prop('checked', 'checked');
    }
    $('#input_teamname_blue').val(game_config.teamname_blue);
    $('#teamname_blue').html(game_config.teamname_blue);
    $('#input_teamname_white').val(game_config.teamname_white);
    $('#teamname_white').html(game_config.teamname_white);
    $("#SelectedGame").val(game_config.game_id);
};

mqtt_messages.onProtocol = function (topic, payload) {
    MQTTdefaultMessages.prototype.onProtocol.call(this, topic, payload);
    update_protocol();
};

mqtt_messages.onGameday = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameday.call(this, topic, payload);
    update_game_options();
};

mqtt_messages.onRoster = function (topic, payload) {
    MQTTdefaultMessages.prototype.onRoster.call(this, topic, payload);
    update_game_options();
};

var update_game_options = function () {
    let inner_html = '<option value="-1" selected>No Game Selected</option>\n';
    if (gameday && roster) {
        for(let i = 0; i < gameday.length; i++) {
            let teamblue = gameday[i]['blue'];
            let teamwhite = gameday[i]['white'];
            if (teamblue in roster) {
                teamblue = roster[teamblue].name;
            }
            if (teamwhite in roster) {
                teamwhite = roster[teamwhite].name;
            }
            inner_html += '<option value="' + gameday[i]['id'] + '">';
            inner_html += '[id=' + gameday[i]['id'] + ']  "';
            inner_html += teamblue + '"  versus  "' + teamwhite + '" (';
            inner_html += plan_formatter(gameday[i]['plan'], '', '', '') + ')';
            inner_html += '</option>\n';
        }
    }
    $('#SelectedGame').html(inner_html);
    if (game_config) {
        $('#SelectedGame').val(game_config.game_id);
    }
};

function update_protocol () {
    if (!protocol) return;
    let inner_html = "";
    for (let i = protocol.length - 1; i >= 0; i--) {
        let game_stop = protocol[i];
        inner_html +=
            `<div class="row align-items-center border-top mt-2 pt-1 border-secondary prot-gs-id${game_stop.id} prot-gamestop">` +
            `<div class="col-4 lead font-weight-bold prot-time">${game_stop.gametime} (${game_stop.halftime})</div>` +
            `<div class="col-8 text-right">` +
            `<button class="btn btn-secondary" type="button" onclick="open_select_event_modal('${game_stop.id}')">Add Event</button></div></div>`;
        for (let j = game_stop.events.length - 1; j >= 0; j--) {
            let event = game_stop.events[j];
            let color_class = '';
            if (event.teamcolor == 'blue') {
                color_class = 'Blue';
            } else if (event.teamcolor == 'white') {
                color_class = 'White';
            }
            inner_html +=
                `<div class="row align-items-center ${color_class} m-1 prot-gs-id${game_stop.id} prot-e-id${event.id}">`;
            if (with_linked_timer.includes(event.event_type)) {
                inner_html += `<div class="col-2 prot-extra text-center linkedtimer-extra" data-id="${event.timer_id}"></div>`;
            } else if (event.event_type == 'timeout') {
                inner_html += `<div class="col-2 prot-extra text-center timeout-extra" data-id="${event.timer_id}"></div>`;
            } else if (event.event_type == 'halftimebreak') {
                inner_html += `<div class="col-2 prot-extra text-center halftimebreak-extra" data-id="${event.timer_id}"></div>`;
            } else if (event.event_type == 'penaltythrow') {
                inner_html += `<div class="col-2 prot-extra text-center penaltythrow-extra" data-id="${event.timer_id}"></div>`;
            } else {
                inner_html +=
                    `<div class="col-2 prot-extra text-center">${event.extra}</div>`;
            }
            inner_html +=
                `<div class="col-1 prot-number">${event.player_nr}</div>
<div class="col-7 prot-text">
  <span class="font-weight-bold lead mr-3 prot-ddisabled="disabled"esc">${event.desc}</span>
  <span class="prot-comment">${event.comment}</span>
</div>
<div class="col-2">
  <button type="button" class="btn btn-secondary py-1 px-3 m-1" onclick="delete_event(${game_stop.id},${event.id})">
    <i class="fa fa-trash uwr-icons"></i></button>
  <button type="button" class="btn btn-secondary py-1 px-3 m-1" onclick="edit_event(${game_stop.id}, ${event.id})">
    <i class="fa fa-pencil uwr-icons"></i></button>
</div></div>`;
        }
    }
    $("#protocol-list").html(inner_html);
    update_linked_timers();
}

function update_linked_timers () {
    if (linked_timers == null) return;
    if (!protocol) return;
    $('.linkedtimer-extra').each(function(i, obj) {
        if (obj.dataset.id in linked_timers) {
            let fa_icon;
            let set_active;
            let inner_html;
            if (linked_timers[obj.dataset.id].active) {
                fa_icon = 'fa-play';
                set_active = false;
                inner_html = `<span>${linked_timers[obj.dataset.id].countdown}</span> `;
            } else {
                fa_icon = 'fa-pause';
                set_active = true;
                inner_html = `<span class='text-muted' style="text-decoration: line-through;">${linked_timers[obj.dataset.id].countdown}</span> `;
            }
            inner_html += `<button type="button" class="btn btn-secondary" onclick="set_linkedtimer_active(${obj.dataset.id}, ${set_active})">
    <i class="fa ${fa_icon} uwr-icons"></i>
</button>`;
            if (obj.innerHTML != inner_html) obj.innerHTML = inner_html;
        } else {
            if (obj.innerHTM != 'Ended') obj.innerHTML = 'Ended';
        }
    });
    if (!game_state) return;
    $('.timeout-extra').each(function(i, obj) {
        if (game_state.is_timeout && obj.dataset.id == timeout_time.id) {
            if (obj.innerHTML != timeout_time.countdown) {
                obj.innerHTML = timeout_time.countdown;
            }
        } else {
            if (obj.innerHTML != "Ended") obj.innerHTML = "Ended";
        }
    });
    $('.halftimebreak-extra').each(function(i, obj) {
        if (game_state.is_halftime_break && obj.dataset.id == halftimebreak_time.id) {
            if (obj.innerHTML != halftimebreak_time.countdown) {
                obj.innerHTML = halftimebreak_time.countdown;
            }
        } else {
            if (obj.innerHTML != "Ended") obj.innerHTML = "Ended";
        }
    });
    $('.penaltythrow-extra').each(function(i, obj) {
        if (game_state.is_penaltythrow && obj.dataset.id == penaltythrow_time.id) {
            if (obj.innerHTML != penaltythrow_time.countdown) {
                obj.innerHTML = penaltythrow_time.countdown;
            }
        } else {
            if (obj.innerHTML != "Ended") obj.innerHTML = "Ended";
        }
    });
}

function set_linkedtimer_active(timer_id, active) {
    let myObj = {'timer_id': timer_id,
                 'active': active};
    mqtt_messages.send("command/set_linkedtimer_active", JSON.stringify(myObj), false);
}

function get_event_desc(event_type, team_color) {
    let new_desc = event_descriptions[event_type];
    if (need_teamcolor.includes(event_type)) {
        if (event_for_team.includes(event_type)) {
            new_desc = new_desc + ' for ';
        } else {
            new_desc = new_desc + ' against ';
        }
        if (game_config) {
            if (team_color == 'blue') {
                new_desc = new_desc + game_config.teamname_blue;
            } else if (team_color == 'white') {
                new_desc = new_desc + game_config.teamname_white;
            }
        } else {
            new_desc = new_desc + team_color;
        }
    }
    return new_desc;
}

function open_select_event_modal(game_stop_id) {
    new_protocol_event = {'game_stop_id': game_stop_id,
                          'event_type': '',
                          'teamcolor': '',
                          'player_nr': '',
                          'comment': ''};
    if (protocol[game_stop_id]) {
        $("#selectEventModal-header").html(`What happend at ${protocol[game_stop_id].gametime} (${protocol[game_stop_id].halftime})?`)
    } else {
        $("#selectEventModal-header").html("What happend?")
    }
    $("#commentModal").modal("hide");
    $("#teamcolorModal").modal("hide");
    $("#selectEventModal").modal("show");
}

function open_teamcolor_modal(event_type) {
    $("#selectEventModal").modal("hide");
    $("#commentModal").modal("hide");
    $('#teamcolorModal-blue').html(
        `<button type="button" class="btn btn-secondary p-4" onclick="add_event('${event_type}', 'blue')">${get_event_desc(event_type, "blue")}</button>`);
    $('#teamcolorModal-white').html(
        `<button type="button" class="btn btn-secondary p-4" onclick="add_event('${event_type}', 'white')">${get_event_desc(event_type, "white")}</button>`);
    $("#teamcolorModal").modal("show");
}

function add_event(event_type, team_color) {
    new_protocol_event.event_type = event_type;
    new_protocol_event.desc = event_descriptions[event_type];
    new_protocol_event.teamcolor = team_color;
    $("#selectEventModal").modal("hide");
    $("#teamcolorModal").modal("hide");
    if (new_protocol_event.event_type == 'timeout') {
        mqtt_messages.send("command/add_event", JSON.stringify(new_protocol_event), false);
    }
    else {
        is_new_event = true;
        open_comment_modal();
    }
}

function edit_event(game_stop_id, event_id) {
    for (let i = 0; i < protocol[game_stop_id]['events'].length; i++) {
        if (protocol[game_stop_id]['events'][i]['id'] == event_id) {
            new_protocol_event = protocol[game_stop_id]['events'][i];
            break;
        }
    }
    new_protocol_event.game_stop_id = game_stop_id;
    new_protocol_event.event_id = event_id;
    is_new_event = false;
    open_comment_modal();
}

function open_comment_modal() {
    $('#commentModal-EventDesc').html(get_event_desc(new_protocol_event.event_type, new_protocol_event.teamcolor));
    $('#commentModal-PlayerNr').val(new_protocol_event.player_nr);
    $('#commentModal-Comment').val(new_protocol_event.comment);
    if (new_protocol_event.teamcolor == 'blue') {
        $('#commentModal-header').addClass("Blue").removeClass("White");
    } else if (new_protocol_event.teamcolor == 'white') {
        $('#commentModal-header').addClass("White").removeClass("Blue");
    } else {
        $('#commentModal-header').removeClass("Blue").removeClass("White");
    }
    $('#commentModal').on('shown.bs.modal', function (e) {
       $('#commentModal-PlayerNr').focus();
    }).modal('show');
}

function save_desc() {
    let temp_player_nr = $('#commentModal-PlayerNr').val();
    let temp_comment = $('#commentModal-Comment').val();
    if (is_new_event) {
        new_protocol_event.player_nr = temp_player_nr;
        new_protocol_event.comment = temp_comment;
        mqtt_messages.send("command/add_event", JSON.stringify(new_protocol_event), false);
    } else {
        if (temp_player_nr != new_protocol_event.player_nr || temp_comment != new_protocol_event.comment) {
            new_protocol_event.player_nr = temp_player_nr;
            new_protocol_event.comment = temp_comment;
            mqtt_messages.send("command/edit_event", JSON.stringify(new_protocol_event), false);
        }
    }
    $("#commentModal").modal("hide");
}

function delete_event(game_stop_id, event_id) {
    let myObj = {'game_stop_id': game_stop_id,
                 'event_id': event_id};
    mqtt_messages.send("command/delete_event", JSON.stringify(myObj), false);
}


/*
########################
# Events and Publishing
########################
*/

add_event_listeners = function () {
    $('#StartClock').on(touchEvent, function(e) {
        e.preventDefault();
        mqtt_messages.send("command/start_game", '""', false);
    });

    $('#StopClock').on(touchEvent, function(e) {
        e.preventDefault();
        mqtt_messages.send("command/stop_game", '""', false);
    });

    $('#ForceStart').on(touchEvent, function(e) {
        e.preventDefault();
        mqtt_messages.send("command/force_start_game", '""', false);
    });

    $('#ResetGame').on(touchEvent, function(e) {
        e.preventDefault();
        mqtt_messages.send("command/reset_game", '""', false);
    });

    $('#SetConfig').on(touchEvent, function(e) {
        e.preventDefault();
        var myObj= {"game_length": $('#game_length').val(),
                    "extra_game_length": $('#extra_game_length').val(),
                    "penaltytime_length": $('#penaltytime_length').val(),
                    "penaltythrow_length": $('#penaltythrow_length').val(),
                    "timeout_length": $('#timeout_length').val(),
                    "matchpenalty_length": $('#matchpenalty_length').val(),
                    "halftime_break_length": $('#halftime_break_length').val(),
                    "n_halftime_total": $('#n_halftime_total').val(),
                    "continuous_time": $('input[name="continuous_time"]:checked').val()};
        mqtt_messages.send("command/configure", JSON.stringify(myObj), false);
        $('#settings-menu').collapse("hide");
    });

    $('#SetDefault').on(touchEvent, function(e) {
        e.preventDefault();
        var myObj= {"game_length": 900,
                    "extra_game_length": 900,
                    "penaltytime_length": 120,
                    "penaltythrow_length": 45,
                    "timeout_length": 60,
                    "matchpenalty_length": 300,
                    "halftime_break_length": 300,
                    "n_halftime_total": 2,
                    "continuous_time": false};
        mqtt_messages.send("command/configure", JSON.stringify(myObj), false);
        $('#settings-menu').collapse("hide");
    });

    $('#SetTeamNames').on(touchEvent, function(e) {
        e.preventDefault();
        let myObj = {"teamname_blue": $('#input_teamname_blue').val(),
                     "teamname_white": $('#input_teamname_white').val()};
        mqtt_messages.send("command/configure", JSON.stringify(myObj), false);
        $('#teamname-menu').collapse("hide");
    });

    $('#SetGameID').on(touchEvent, function(e) {
        e.preventDefault();
        let gameID = $('#SelectedGame').val();
        let myobj = {"new_id":gameID};
        mqtt_messages.send("command/select_game", JSON.stringify(myobj), false);
        $('#teamname-menu').collapse("hide");
    });

    $('#ToggleExtraHalftime').on(touchEvent, function(e) {
        e.preventDefault();
        let myObj;
        if (game_state && game_state.is_extra_halftime){
            myObj= {"turn_on":"false"};
        }
        else{
            myObj= {"turn_on":"true"};
        }
        mqtt_messages.send("command/set_extra_halftime", JSON.stringify(myObj), false);
    });
};
